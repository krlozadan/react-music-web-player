# Music Web Player

Author: Carlos Adan Cortes De la Fuente
Date: 09/28/2018
Tou can find the web media player [here] (https://vfs-music-player.firebaseapp.com/)

## Synopsis
---------------
Responsive Web Music Player made using:
- [React](https://reactjs.org/)
- [Howler](https://howlerjs.com/)
- [Firebase](https://reactjs.org/)
- [Font Awesome](https://fontawesome.com/)