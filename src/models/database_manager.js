import firebase from 'firebase/app';
import 'firebase/firestore';

// Initialize App
var config = {
    apiKey: "AIzaSyDyciD2XzFwknBv_xeHBqTKJ0sXcl3Ddpw",
    authDomain: "vfs-music-player.firebaseapp.com",
    databaseURL: "https://vfs-music-player.firebaseio.com",
    projectId: "vfs-music-player",
    storageBucket: "vfs-music-player.appspot.com",
    messagingSenderId: "935249096874"
};

class DatabaseManager {
    constructor() {
        firebase.initializeApp(config);
        // Initialize Cloud Firestore through Firebase
        this.db = firebase.firestore();
        // Disable deprecated features
        this.db.settings({ timestampsInSnapshots: true });
    }

    getAllSongs(callback) {
        this.db.collection('songs').get().then((querySnapshot) => {
            let songList = [];
            querySnapshot.forEach(doc => songList.push({ id : doc.id, ...doc.data() }));
            callback(songList);
        });
    }

    addSongToPlaylist(songID) {
        return this.db.collection('songs').doc(songID).update({ in_playlist : true });
    }
    
    removeSongFromPlaylist(songID) {
        return this.db.collection('songs').doc(songID).update({ in_playlist : false });
    }
}

export default DatabaseManager;