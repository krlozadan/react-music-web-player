import { Howl } from 'howler';
import { PANELS } from '../components/top_bar';

export const PLAYER_STATE = {
    NO_SONG_SELECTED : 0,
    PLAYING : 1,
    PAUSED : 2
};

class MediaPlayer {

    constructor() {
        this.track = null;
        this.songTracker = null;
        this.trackStatus = 0;
    }

    getPreviousTrackIndex(selectedSong, songs, activePanelWhenSelected) {
        let songIndex = songs.indexOf(selectedSong);
        
        if (songIndex == 0) {
            return 0;
        }

        if (activePanelWhenSelected == PANELS.ALL_SONGS) {    
            return --songIndex;   
        }

        if (activePanelWhenSelected == PANELS.PLAYLIST) {
            let newIndex = songIndex;
            do {
                if (songs[--songIndex].in_playlist) {
                    newIndex = songIndex;
                    break;
                } 
            } while(songIndex > 0);
            return newIndex;
        }
    }

    getNextTrackIndex(selectedSong, songs, activePanelWhenSelected) {
        let songIndex = songs.indexOf(selectedSong);
        
        if (songIndex == songs.length - 1) { 
            this.resetTrackState();
            return -1; 
        }

        if (activePanelWhenSelected == PANELS.ALL_SONGS) {    
            return ++songIndex;   
        }

        if (activePanelWhenSelected == PANELS.PLAYLIST) {

            if (songIndex == songs.length - 2 && songs[songIndex + 1].in_playlist == false) {
                this.resetTrackState();
                return -1;
            }
            
            let newIndex = songIndex;
            do {
                if (songs[++songIndex].in_playlist) {
                    newIndex = songIndex;
                    break;
                } 
            } while(songIndex < songs.length - 1);
            return newIndex;
        }
    }

    resetTrackState() {
        if (this.track != null) { 
            clearInterval(this.songTracker);
            this.trackStatus = 0;
            this.track.unload();
        }
    }

    loadTrack(songURL, playCallback, endCallback) {
        this.resetTrackState();
        this.track = new Howl({
            src : songURL,
            volume : 1,
            html5 : true,
            preload : true,
            autoplay : true
        });
        
        this.track.once('play', () => {
            playCallback(PLAYER_STATE.PLAYING);
            this.songTracker = setInterval(() => this.trackStatus = this.track.seek() / this.track.duration(), 1000);
        });

        this.track.once('end', () => endCallback());
    }

    pause(callback) { 
        this.track.pause();
        callback(PLAYER_STATE.PAUSED);
    }

    resume(callback) {
        this.track.play();
        callback(PLAYER_STATE.PLAYING);
    }
}

export default MediaPlayer;