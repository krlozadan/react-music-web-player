import React from 'react';
import SongListItem from './song_list_item';
import { PANELS } from './top_bar';

const SongList = ({ onAddSongToPlaylist, onRemoveSongFromPlaylist, onSongSelect, songs, activePanel }) => {

    let songItems = [];

    if (activePanel == PANELS.ALL_SONGS) {
        songItems = songs.map(song => <SongListItem onAddSongToPlaylist={ onAddSongToPlaylist } onRemoveSongFromPlaylist={ onRemoveSongFromPlaylist } onSongSelect={ onSongSelect } key={ song.id } song={ song } />);
    } else if (activePanel == PANELS.PLAYLIST) {
        songItems = songs.filter(song => song.in_playlist).map(song => <SongListItem  onRemoveSongFromPlaylist={ onRemoveSongFromPlaylist } onSongSelect={ onSongSelect } key={ song.id } song={ song } />);
    }

    return (
        <div className="song-list">{ songItems }</div>
    );
}

export default SongList;