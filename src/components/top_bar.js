import React from 'react';

export const PANELS = {
    ALL_SONGS : 0,
    PLAYLIST : 1
};

const TopBar = ({ activePanel, onPanelSelect }) => {
    
    const title = activePanel == PANELS.ALL_SONGS ? 'All Songs' : 'Your Playlist';

    return (
        <div>
            <div className="horizontal-flex">
                <div className="col text-center"><button onClick={ () => onPanelSelect(PANELS.ALL_SONGS) } className="mp-btn"><i className="fa fa-music"></i></button></div>
                <div className="col text-center"><button onClick={ () => onPanelSelect(PANELS.PLAYLIST) } className="mp-btn"><i className="fa fa-list"></i></button></div>
            </div>
            <p className="playlist-name">{ title }</p>
        </div>
    );
};

export default TopBar;