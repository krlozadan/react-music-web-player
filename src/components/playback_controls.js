import React, { Component } from 'react';
import { PLAYER_STATE } from '../models/media_player';

class PlaybackControls extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        if (this.props.mediaPlayerState == PLAYER_STATE.NO_SONG_SELECTED) return (null);
        const playPauseButton = this.displayPauseOrPlay();
        return (
            <div>
                <div>
                    <p className="song-info big">{ this.props.song.name }</p>
                    <p className="song-info">{ this.props.song.artist }</p>
                </div>
                <div>
                    <p className="song-info">{ this.props.song.duration }</p>
                    <div className="track-progress-bar"><i style={ { left : (this.props.trackStatus * 100) + '%' } } className="fa fa-circle track-progress-indicator"></i></div>
                </div>
                <div className="horizontal-flex">
                    <div><button className="mp-btn" onClick={ () => this.props.onStepBackward() }><i className="fa fa-step-backward"></i></button></div>
                    { playPauseButton }
                    <div><button className="mp-btn" onClick={ () => this.props.onStepForward() }><i className="fa fa-step-forward"></i></button></div>
                </div>
            </div>
        );
    }

    displayPauseOrPlay() {
        if (this.props.mediaPlayerState == PLAYER_STATE.PLAYING) {
            return (<div><button className="mp-btn" onClick={ () => this.props.onPause() }><i className="fa fa-pause"></i></button></div>);
        } else if (this.props.mediaPlayerState == PLAYER_STATE.PAUSED) {
            return (<div><button className="mp-btn" onClick={ () => this.props.onResume() }><i className="fa fa-play"></i></button></div>);
        } else {
            return (null);
        }
    }
};

export default PlaybackControls;