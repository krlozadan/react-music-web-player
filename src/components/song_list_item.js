import React from 'react';

const SongListItem = ({ song, onSongSelect, onAddSongToPlaylist, onRemoveSongFromPlaylist }) => {

    const playlistButton = song.in_playlist ? <button onClick={ () => onRemoveSongFromPlaylist(song) }><i className="fa fa-check"></i></button> : <button onClick={ () => onAddSongToPlaylist(song) }><i className="fa fa-plus"></i></button>;

    return (
        <div className="song-list-item">
            <p onClick={ () => onSongSelect(song) }>{ song.name } <small><i>by { song.artist }</i></small></p>  
            { playlistButton }
        </div>
    );
};

export default SongListItem;