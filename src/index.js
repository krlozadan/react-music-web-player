import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import TopBar, { PANELS } from './components/top_bar';
import PlaybackControls from './components/playback_controls';
import SongList from './components/song_list';
import DatabaseManager from './models/database_manager';
import MediaPlayer, { PLAYER_STATE } from './models/media_player';

class App extends Component {
    
    constructor(props) {
        super(props);
        this.dbManager = new DatabaseManager();
        this.mediaPlayer = new MediaPlayer(); 
        this.state = {
            songs : [],
            selectedSong : null,
            trackStatus : 0,
            activePanelWhenSelected : PLAYER_STATE.NO_SONG_SELECTED,
            activePanel : PANELS.ALL_SONGS,
            mediaPlayerState : PLAYER_STATE.NO_SONG_SELECTED
        };  
    }

    componentDidMount() {
        this.dbManager.getAllSongs((songs) => this.setState({ songs }));
        setInterval(() => this.setState({ trackStatus : this.mediaPlayer.trackStatus }), 1000);
    }

    render() {
        return (
            <div className="app-container">
                <div className="top-content">
                    <TopBar activePanel={ this.state.activePanel } onPanelSelect={ selectedPanel =>  this.switchPanel(selectedPanel) }/>
                </div>
                <div className="center-content">
                    <SongList onAddSongToPlaylist={ addedSong => this.addSongToPlaylist(addedSong) } onRemoveSongFromPlaylist={ removedSong => this.removeSongFromPlaylist(removedSong) } onSongSelect={ selectedSong => this.onSongSelect(selectedSong) } songs={ this.state.songs } activePanel={ this.state.activePanel }/>
                </div>
                <div className="bottom-content">
                    <PlaybackControls trackStatus={ this.state.trackStatus } mediaPlayerState={ this.state.mediaPlayerState } song={ this.state.selectedSong } onPause={ onPause => this.onPause() } onResume={ onResume => this.onResume() } onStepBackward={ onStepBackward => this.onStepBackward() } onStepForward={ onStepForward => this.onStepForward() }/>
                </div>
            </div>
        );
    }

    switchPanel(newPanel) {
        if (newPanel == this.state.activePanel) return;
        this.setState({ activePanel : newPanel });
    }

    addSongToPlaylist(song) {
        if (song.in_playlist) return;
        const index = this.state.songs.indexOf(song);
        this.dbManager.addSongToPlaylist(song.id).then(() => {
            let newSongsArray = [...this.state.songs];
            newSongsArray[index].in_playlist = true;
            this.setState({ songs : newSongsArray });
        });
    }
        
    removeSongFromPlaylist(song) {
        if (song.in_playlist == false) return;
        const index = this.state.songs.indexOf(song);
        this.dbManager.addSongToPlaylist(song.id).then(() => {
            let newSongsArray = [...this.state.songs];
            newSongsArray[index].in_playlist = false;
            this.setState({ songs : newSongsArray });
        });
    }
    
    /* Playback Callback Methods */
    onSongSelect(song) {
        this.mediaPlayer.loadTrack(song.url, (mediaPlayerState) => this.setState({ selectedSong : song, mediaPlayerState, activePanelWhenSelected : this.state.activePanel }), () => this.onStepForward());
    }

    onPause() {
        this.mediaPlayer.pause((mediaPlayerState) => this.setState({ mediaPlayerState }));
    }
    
    onResume() {
        this.mediaPlayer.resume((mediaPlayerState) => this.setState({ mediaPlayerState }));
    }

    onStepBackward() {
        let previousTrackIndex = this.mediaPlayer.getPreviousTrackIndex(this.state.selectedSong, this.state.songs, this.state.activePanelWhenSelected);
        this.mediaPlayer.loadTrack(this.state.songs[previousTrackIndex].url, (mediaPlayerState) => this.setState({ selectedSong : this.state.songs[previousTrackIndex], mediaPlayerState }));
    }

    onStepForward() {
        let nextTrackIndex = this.mediaPlayer.getNextTrackIndex(this.state.selectedSong, this.state.songs, this.state.activePanelWhenSelected);
        if (nextTrackIndex != -1) {
            this.mediaPlayer.loadTrack(this.state.songs[nextTrackIndex].url, (mediaPlayerState) => this.setState({ selectedSong : this.state.songs[nextTrackIndex], mediaPlayerState }));
        } else {
            this.setState({
                selectedSong : null,
                activePanelWhenSelected : PLAYER_STATE.NO_SONG_SELECTED,
                mediaPlayerState : PLAYER_STATE.NO_SONG_SELECTED
            });
        }
    }
}

ReactDOM.render(<App />, document.querySelector('#app'));